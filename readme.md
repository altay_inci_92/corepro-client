# CORAL - CLIENT


### USAGE
    
     from coral_client import Bookadapter
     
     params = {'checkin': '2016-10-10', 'checkout': '2016-10-12',
              'pax': '1', 'destination_code': '182cc',
              'client_nationality': 'tr', 'currency': 'USD'}
     
     obje=Bookadapter(params,"USERNAME","PASSWORD")
     
### Search

     obje.search()
    
     Response:
     {u'code': u'2881398bf8e14389bc845728739e0363',
         u'count': 3,
         u'next_page_code': None,
         u'results': [{u'checkin': u'2016-10-10',
           u'checkout': u'2016-10-12',
           u'destination_code': u'182cc',
           u'hotel_code': u'1117f3',
           u'products': [{u'code': u'ERfzIUoRIAAAAAAAAAAAAAAAAAAAAAAAAAHAKIE5i_jhQ4m8hFcoc54DY4AAAAAAAAAAAAAAAA8PwAAAABBcAADOgOT_478ot3BrAAIBAAAAAAAAAAAABA',
             u'cost': u'167.52',
             u'currency': u'USD',
             u'hotel_currency': None,
             u'hotel_price': None,
             u'list_price': u'203.40',
             u'meal_type': u'BC',
             u'minimum_selling_price': None,
             u'nonrefundable': None,
             u'offer': False,
             u'pay_at_hotel': False,
             u'policies': [],
             u'price': u'167.52',
             u'providers': [u'tourico'],
             u'rooms': [{u'pax': {u'adult_quantity': 1, u'children_ages': []},
               u'room_category': u'Superior',
               u'room_description': u'Superior + Breakfast',
               u'room_type': u'SB'}],
             u'supports_cancellation': True,
             u'view': False}
             
### Availability

     obje.availability('ERfzIUoRIAAAAAAAAAAAAAAAAAAAAAAAAAHAKIE5i_jhQ4m8hFcoc54DY4AAAAAAAAAAAAAAAA8PwAAAABBcAADOgOT_478ot3BrAAIBAAAAAAAAAAAABA')
    
### Provision
    
     obje.provision('ERfzIUoRIAAAAAAAAAAAAAAAAAAAAAAAAAHAKIE5i_jhQ4m8hFcoc54DY4AAAAAAAAAAAAAAAA8PwAAAABBcAADOgOT_478ot3BrAAIBAAAAAAAAAAAABA')
     
     
     

Pax information for booking ->  pax = {'name':'1,altay,inci,adult'}


### Book
    
     obje.book('AHDAHDGDHSGAH',pax)

### Cancel

     obje.cancel('FDSASDDASADAS')
     
### Bookings

     obje.bookings()
     
