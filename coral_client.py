import requests
import urlparse

class Bookadapter(object):
    def __init__(self, params, username, password):

        self.params = params
        self.base_url = 'http://localhost:8000/api/v2/'
        self.username = username
        self.password = password

    def search(self):

        search_url = urlparse.urljoin(self.base_url, 'search/') + '?'
        for i in range(0, len(self.params.keys())):
            search_url += '%s=%s' % (
                self.params.keys()[i], self.params.values()[i])
            search_url += '&'

        r = requests.get(search_url, auth=(self.username, self.password))

        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)

    def availability(self, code):

        availability_url = urlparse.urljoin(self.base_url, 'availability/%s' % code)

        r = requests.get(availability_url, auth=(self.username, self.password))
        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)

    def provision(self, code):

        provision_url = urlparse.urljoin(self.base_url, 'provision/%s' % code)
        r = requests.post(provision_url, auth=(self.username, self.password))

        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)

    def book(self, code, pax):

        book_url = urlparse.urljoin(self.base_url, 'book/%s' % code)
        r = requests.post(book_url, data=pax,
                          auth=(self.username, self.password))

        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)

    def cancel(self, code):

        cancel_url = urlparse.urljoin(self.base_url, 'cancel/%s' % code)
        r = requests.post(cancel_url, auth=(self.username, self.password))

        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)

    def bookings(self):

        bookings_url = urlparse.urljoin(self.base_url, 'bookings')

        r = requests.get(bookings_url, auth=(self.username, self.password))

        if r.status_code == 200:
            return r.json()
        else:
            raise ValueError(r.json(), r.status_code)
