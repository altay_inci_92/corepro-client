from unittest import TestCase, main
from coral_client import Bookadapter


class Test(TestCase):
    params = {'checkin': '2016-10-10', 'checkout': '2016-10-12',
              'pax': '1', 'destination_code': '182cc',
              'client_nationality': 'tr', 'currency': 'USD'}

    pax = {'name': '1,dfdf,asasa,adult'}
    obje2 = Bookadapter(params, 'USERNAME', 'PASSWORD')
    search_response = obje2.search()
    product_code = search_response['results'][0]['products'][0]['code']

    def __init__(self, *args, **kwargs):
        super(Test, self).__init__(*args, **kwargs)

    def test_search(self):
        self.assertGreaterEqual(self.search_response['count'], 1)
        self.assertTrue(isinstance(self.search_response['results'], list))
        self.assertIn('code',
                      self.search_response['results'][0]['products'][0])

    def test_availability(self):
        resp = self.obje2.availability(self.product_code)
        self.assertIsInstance(resp, dict)
        self.assertIn('meal_type', resp)
        self.assertIn('price', resp)
        self.assertIn('cost', resp)
        self.assertIsInstance(resp['policies'], list)

    def test_provision(self):
        resp = self.obje2.provision(self.product_code)
        self.assertIn('code', resp)

    def test_book_and_cancel(self):
        obje2_book_code = self.obje2.provision(self.product_code)
        resp = self.obje2.book(obje2_book_code['code'], self.pax)
        self.assertEqual('succeeded', resp['status'])
        resp = self.obje2.cancel(resp['code'])
        self.assertIsInstance(resp, dict)
        self.assertIn('code', resp)

    def test_bookings(self):
        resp = self.obje2.bookings()
        self.assertIsInstance(resp, list)

if __name__ == '__main__':
    main()
