from setuptools import find_packages, setup

setup(
    name='corepro-client',
    version='1.0.0',
    packages=find_packages(),
    url='https://bitbucket.org/altay_inci_92/corepro-client',
    license='GPL',
    author='Altay Inci',
    author_email='altay.inci@metglobal.com',
    description='Coral - Book API',
    install_requires=['requests'],
    classifiers=(
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ),
)
