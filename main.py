from coral_client import Bookadapter

params = {'checkin': '2016-10-10', 'checkout': '2016-10-12',
          'pax': '1',
          'hotel_code': '10e86a',
          'client_nationality': 'tr',
          'currency': 'USD',
          }

Booker = Bookadapter(params, 'USERNAME', 'PASSWORD')
search_resp = Booker.search()
product_code = search_resp['results'][0]['products'][0]['code']

print 'availibility'
print '-' * 44
print Booker.availability(product_code)

print 'provision'
print '-' * 44
print Booker.provision(product_code)
provision_code = Booker.provision(product_code)

print 'book'
print '-' * 44
book_params = {'name': '1,adsad,adasda,adult'}
book_response = Booker.book(provision_code['code'], book_params)
print book_response
book_code = book_response['code']
print 'cancel'
print '-' * 44
print Booker.cancel(book_code)
print 'bookings'
print '-' * 44
print Booker.bookings()
